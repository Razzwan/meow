<?php
/**
 * all routes
 */
/** @var Meow\Application $application */
use Meow\Routing\Route;


$this->getRouting()->add(new Route('/', function () {
    return new \Meow\Http\Response('Meow Meow');
}))->math('GET|POST');
