<?php
namespace app\main\controllers;

/**
 * optional
 * Class MainController
 * @package app\main
 */
class MainController extends \meow\core\Controller
{
    public function indexAction()
    {
        //render index view with all src folder
        return $this->render();
    }
}
